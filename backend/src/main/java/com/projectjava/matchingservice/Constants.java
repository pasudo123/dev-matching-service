package com.projectjava.matchingservice;

public class Constants {
    // Auth
    public static final String HEADER_AUTH = "Bearer";

    // http response code
    public static final int HTTP_CODE_OK = 200;
    public static final int HTTP_CODE_CREATED = 201;
    public static final int HTTP_CODE_BAD_REQUEST = 400;
    public static final int HTTP_CODE_FORBIDDEN = 403;
    public static final int HTTP_CODE_NOT_FOUND = 404;
    public static final int HTTP_CODE_METHOD_NOT_ALLOWED = 405;

    public static final String MSG_SUCCESS = "SUCCESS";
    public static final String MSG_FAIL= "FAIL";
    public static final String MSG_NO_DATA= "NO DATA";

    // JWT auth
    public static final String AUTH_TOKEN_VALIDATION = "tokenValidation";
    public static final String AUTH_REPONSE_MSG_SUCCESS = "success";
    public static final String AUTH_REPONSE_MSG_SUCCESS_TOKEN_VALID = "success - token valid";
    public static final String AUTH_REPONSE_MSG_FAIL = "fail";
    public static final String AUTH_REPONSE_MSG_FAIL_TOKEN_NULL = "fail - token null";
    public static final String AUTH_REPONSE_MSG_FAIL_TOKEN_INVALID = "fail - token invalid";
}

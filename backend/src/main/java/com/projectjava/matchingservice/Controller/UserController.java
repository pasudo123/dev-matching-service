package com.projectjava.matchingservice.Controller;

import com.projectjava.matchingservice.NetworkService.NetResponseDTO;
import com.projectjava.matchingservice.UserService.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @PostMapping("/user/check")
    @ResponseBody
    public NetResponseDTO checkUser(HttpServletRequest req) throws Exception {
        String userId = req.getParameter("id");
        String userPw = req.getParameter("pw");

        int saveResult = userService.loginUser(userId, userPw);

        Map<String, Object> resultData = new HashMap<String, Object>();
        resultData.put("login id",userId);

        if(saveResult == 1){
            resultData.put("result", "signin success type : " + saveResult);
        }else{
            resultData.put("result", "signin fail type : " + saveResult);
        }

        return new NetResponseDTO(resultData);
    }

}

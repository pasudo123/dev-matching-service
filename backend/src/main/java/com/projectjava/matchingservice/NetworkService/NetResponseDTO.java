package com.projectjava.matchingservice.NetworkService;

import com.projectjava.matchingservice.Constants;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class NetResponseDTO {
    int httpCode = 404;
    String msg = "not fount";
    Map<String, Object> data = null;

    public NetResponseDTO(int httpCode, String msg) {
        this.httpCode = httpCode;
        this.msg = msg;
    }

    public NetResponseDTO(Map<String, Object> data) {
        if(data == null){
            this.httpCode = Constants.HTTP_CODE_NOT_FOUND;
            this.msg = Constants.MSG_FAIL;
        }else{
            this.httpCode = Constants.HTTP_CODE_OK;
            this.msg = Constants.MSG_SUCCESS;
            this.data = data;
        }
    }
}

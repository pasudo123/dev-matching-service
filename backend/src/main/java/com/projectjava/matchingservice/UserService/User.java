package com.projectjava.matchingservice.UserService;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userNum;

    @Column(length = 20, nullable = false)
    private String userId;

    @Column(length = 20, nullable = false)
    private String userPw;

    @CreationTimestamp
    private Date createTime;

    @Column(length = 1, columnDefinition = "CHAR(1) default 'N'")
    private String deleteYn;

    @Builder
    public User(String userId, String userPw) {
        this.userId = userId;
        this.userPw = userPw;
    }

//    @Id
//    @Column(name = "user_number")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long number;
//
//    @Column(name = "user_id", length = 30, unique = true, nullable = false)
//    private String userId;
//
//    @JsonIgnore
//    @NotNull
//    @Size(min = 4, max = 60)
//    @Column(name = "password_hash", length = 60, nullable = false)
//    private String userPw;
//
//    @CreatedDate
//    @Column(name = "create_time")
//    @JsonIgnore
//    private LocalDateTime createTime = LocalDateTime.now();
//
//    @LastModifiedDate
//    @Column(name = "last_login")
//    @JsonIgnore
//    private LocalDateTime lastLoginDate = LocalDateTime.now();
//
//    @Column(name = "is_enable", nullable = false)
//    @JsonIgnore
//    private int isEnable = 0;
//
//    @Column(name = "is_deleted", nullable = false)
//    @JsonIgnore
//    private boolean isDeleted = false;
//
//    @Builder
//    public User(String id, @NotNull @Size(min = 4, max = 60) String password) {
//        this.userId = id;
//        this.userPw = password;
//    }

}

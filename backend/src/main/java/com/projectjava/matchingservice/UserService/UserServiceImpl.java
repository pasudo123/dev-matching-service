package com.projectjava.matchingservice.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public int loginUser(String id, String pw) {
        User userInfo = userRepository.findByUserId(id);
        if( userInfo != null ){
            return 1;
        }
        return -1;
    }
}

-- sample
--CREATE TABLE IF NOT EXISTS user
--(
--    `user_num`     INT            NOT NULL    AUTO_INCREMENT COMMENT '회원 번호',
--    `user_id`      VARCHAR(45)    NOT NULL    COMMENT '회원 아이디',
--    `user_pw`      VARCHAR(45)    NOT NULL    COMMENT '회원 비밀번호',
--    `create_time`  DATETIME       NULL        COMMENT '생성 시간',
--    `delete_yn`    CHAR(1)        NOT NULL    COMMENT '삭제 여부',
--    PRIMARY KEY (user_id)
--);

INSERT INTO user (user_id, user_pw, create_time, delete_yn) VALUES ('user_id 1', 'user_pw 1', NOW(), 'N');
INSERT INTO user (user_id, user_pw, create_time, delete_yn) VALUES ('user_id 2', 'user_pw 2', NOW(), 'N');
INSERT INTO user (user_id, user_pw, create_time, delete_yn) VALUES ('user_id 3', 'user_pw 3', NOW(), 'N');
INSERT INTO user (user_id, user_pw, create_time, delete_yn) VALUES ('user_id 4', 'user_pw 4', NOW(), 'N');
INSERT INTO user (user_id, user_pw, create_time, delete_yn) VALUES ('user_id 5', 'user_pw 5', NOW(), 'N');
package com.projectjava.matchingservice.repository;

import com.projectjava.matchingservice.UserService.User;
import com.projectjava.matchingservice.UserService.UserRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @After
    public void cleanup() {
        userRepository.deleteAll();
    }

    @Test
    public void userRepoTest(){
        userRepository.save(User.builder()
                                .userId("user1")
                                .userPw("user1")
                                .build());

        User userInfo = userRepository.findByUserId("user1");
        assertThat(userInfo.getUserId(), is("user1"));

        List<User> userList = userRepository.findAll();
        //then
        userInfo = userList.get(5);
        assertThat(userInfo.getUserId(), is("user1"));
        assertThat(userInfo.getUserPw(), is("user1"));
    }

}

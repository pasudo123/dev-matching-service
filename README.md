# dev-matching-service

## 개요
프로젝트를 매칭하는 웹서비스 제작.

## 기능
- 회원관리
- 프로젝트 관리
- 댓글관리

## Front End
- 담당자 : 박성동
- Vue.js

## Back End
- 담당자 : 윤준모
- Spring Boot
    - Rest API

- Table 구조
    - url : http://aquerytool.com:80/aquerymain/index/?rurl=e8f0fa18-3fbd-4258-a4f7-b5b29f81b791
    - pw : t7n3en
    - 읽기 전용!

## Infra Structure
- 담당자 : 박대호
- AWS
    - Gitlab (CI/CD)
    - Docker